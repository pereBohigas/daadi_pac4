//
//  ViewControllerComplex.swift
//  PR4S
//
//  Created by Javier Salvador Calvo on 12/12/16.
//  Copyright © 2016 UOC. All rights reserved.
//

import UIKit
import Foundation
import MapKit

import AVFoundation
import AVKit

import MobileCoreServices

class ViewControllerComplex: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    var m_item:CItemData?
    var m_locationManager:CLLocationManager?
    var m_map:MKMapView?
    
    var player:AVPlayer?
    var m_AVPlayerLayer:AVPlayerLayer?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // BEGIN-CODE-UOC-2
        let screenSize = UIScreen.main.bounds
        let buttonBarHeight: CGFloat = 80
        
        self.view.backgroundColor = UIColor.black
        
        // Initialize map view
        self.m_map = MKMapView()
        self.m_map!.translatesAutoresizingMaskIntoConstraints = false
        self.m_map!.delegate = self
        self.view.addSubview(self.m_map!)
        
        // Set map view constraints and markers
        self.m_map!.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.m_map!.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.m_map!.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        self.m_map!.heightAnchor.constraint(equalToConstant: screenSize.height/2).isActive = true
        self.AddMarkers()
        
        // Create button bar
        let buttonBar = UIImageView()
        buttonBar.translatesAutoresizingMaskIntoConstraints = false
        buttonBar.isUserInteractionEnabled = true
        self.view.addSubview(buttonBar)
        
        // Set button bar constraints
        buttonBar.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        buttonBar.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        buttonBar.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        buttonBar.heightAnchor.constraint(equalToConstant: buttonBarHeight).isActive = true
        
        // Create play button
        let playButton = UIButton()
        playButton.setImage(UIImage.init(named: "play"), for: .normal)
        playButton.addTarget(self, action: #selector(Play(sender:)), for: UIControl.Event.touchDown)
        playButton.translatesAutoresizingMaskIntoConstraints = false
        buttonBar.addSubview(playButton)

        // Set play button constraints
        playButton.leadingAnchor.constraint(equalTo: buttonBar.leadingAnchor).isActive = true
        playButton.heightAnchor.constraint(equalToConstant: buttonBarHeight).isActive = true
        playButton.widthAnchor.constraint(equalToConstant: buttonBarHeight).isActive = true
        
        // Create pause button
        let pauseButton = UIButton()
        pauseButton.setImage(UIImage.init(named: "pause"), for: .normal)
        pauseButton.addTarget(self, action: #selector(Pause(sender:)), for: UIControl.Event.touchDown)
        pauseButton.translatesAutoresizingMaskIntoConstraints = false
        buttonBar.addSubview(pauseButton)
        
        // Set pause button constraints
        pauseButton.leadingAnchor.constraint(equalTo: playButton.trailingAnchor).isActive = true
        pauseButton.heightAnchor.constraint(equalToConstant: buttonBarHeight).isActive = true
        pauseButton.widthAnchor.constraint(equalToConstant: buttonBarHeight).isActive = true
        
        // Create video frame
        let videoFrame = UIImageView.init(image: UIImage.init(named: "tv"))
        videoFrame.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(videoFrame)
        
        // Set video frame constraints
        videoFrame.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        videoFrame.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        videoFrame.topAnchor.constraint(equalTo: self.m_map!.bottomAnchor, constant: 0).isActive = true
        videoFrame.bottomAnchor.constraint(equalTo: buttonBar.topAnchor, constant: 0).isActive = true
        
        // Create video layer
        let videoLayer = UIView()
        videoLayer.translatesAutoresizingMaskIntoConstraints = false
        self.view.insertSubview(videoLayer, belowSubview: videoFrame)
        
        // Set video layer
        videoLayer.leadingAnchor.constraint(equalTo: videoFrame.leadingAnchor, constant: 0).isActive = true
        videoLayer.trailingAnchor.constraint(equalTo: videoFrame.trailingAnchor, constant: 0).isActive = true
        videoLayer.topAnchor.constraint(equalTo: videoFrame.topAnchor, constant: 0).isActive = true
        videoLayer.bottomAnchor.constraint(equalTo: videoFrame.bottomAnchor, constant: 0).isActive = true
        
        // Set AVPlayer
        self.player = AVPlayer()
        self.m_AVPlayerLayer = AVPlayerLayer(player: player)
        self.m_AVPlayerLayer?.frame = videoFrame.bounds
        videoLayer.layer.addSublayer(self.m_AVPlayerLayer!)
        // END-CODE-UOC-2
        
        
        
        
        // BEGIN-CODE-UOC-5
        self.m_locationManager = CLLocationManager()
        
        if CLLocationManager.locationServicesEnabled() {
            self.m_locationManager?.delegate = self
            self.m_locationManager?.desiredAccuracy = kCLLocationAccuracyBest
            
            if CLLocationManager.authorizationStatus() != .denied || CLLocationManager.authorizationStatus() != .restricted {
                if CLLocationManager.authorizationStatus() == .notDetermined {
                    self.m_locationManager?.requestWhenInUseAuthorization()
                    self.m_locationManager?.startUpdatingLocation()
                }
            }
            
            m_map?.showsUserLocation = true
        }
        // END-CODE-UOC-5
        
        
        // BEGIN-CODE-UOC-6
        self.player?.addObserver(self, forKeyPath:  #keyPath(AVPlayerItem.status), options:  [.new, .old], context: nil)
        // END-CODE-UOC-6
        
    }
    
    // BEGIN-CODE-UOC-8
    @objc func Play(sender:UIButton)
    {
        print("Play button was pressed")
        self.player?.play()
    }

    
    @objc func Pause(sender:UIButton)
    {
        print("Pause button was pressed")
        self.player?.pause()
    }
    // END-CODE-UOC-8
    
    func AddMarkers()
    {
    
    // BEGIN-CODE-UOC-3
        do {
            guard let markersData = m_item!.m_data.data(using: String.Encoding.utf8) else {
                print("Error no data for map markers")
                return
            }
            
            // Parse the received data into an array of dictionaries
            let markersDictionaryArray = try JSONSerialization.jsonObject(with: markersData , options: JSONSerialization.ReadingOptions.mutableContainers) as? [NSMutableDictionary]
            
            // Create marker from dictionary and add it to the map
            for markerDictionary in markersDictionaryArray! where markersDictionaryArray != nil{
                if let newMarker = createMarkerFromDictionary(dictionary: markerDictionary) {
                    self.m_map!.addAnnotation(newMarker)
                    print("Added a marker with the title: \(newMarker.title ?? "no title")")
                }
            }
        } catch {
            print("Error deserializing JSON data: \(error)")
        }
    }
    
    func createMarkerFromDictionary(dictionary: NSMutableDictionary) -> MKMyPointAnnotation? {
        var title: String?
        var lat: Double?
        var long: Double?
        var movieURL: URL?
        
        // Loop over all dictionary keys and, when they match with the expected ones, load their values into a marker object
        for (key, value) in dictionary {
            switch (key as? String) {
            case "lat":
                lat = (value as? Double)
            case "lon":
                long = (value as? Double)
            case "movie":
                if let stringURL: String = value as? String {
                    movieURL = URL(string: stringURL)
                }
            case "title":
                title = (value as? String)
            default:
                break
            }
        }
        
        // If the data has latitude and longitude create a marker
        if let latitude = lat, let longitude = long {
            let marker = MKMyPointAnnotation(title, latitude, longitude, movieURL)
            return marker
        } else {
            return nil
        }
    }
    
    // Custom class to add extra data to MKPointAnnotation
    class MKMyPointAnnotation: MKPointAnnotation {
        override var subtitle: String? {
            willSet { willChangeValue(forKey: "subtitle") }
            didSet { didChangeValue(forKey: "subtitle") }
        }
        var videoURL: URL?
        
        init(_ markerTitle: String?, _ markerLatitude: Double, _ markerLongitude: Double, _ markerVideoURL: URL?) {
            self.videoURL = markerVideoURL
            super.init()
            super.title = markerTitle
            super.coordinate = CLLocationCoordinate2D(latitude: markerLatitude, longitude: markerLongitude)
        }
    // END-CODE-UOC-3
    
    }
    
    // BEGIN-CODE-UOC-7
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if self.player?.status == .readyToPlay {
            self.playVideo()
        } else if self.player?.status == .failed {
            print("An error occured loading the video")
        }
    }
    // END-CODE-UOC-7
    
    
    
    
    // BEGIN-CODE-UOC-4
    // Set view for each marker
    func mapView (_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        print("A view was set for the marker with the title: \(annotation.title! ?? "no title")")
        guard let marker = (annotation as? MKMyPointAnnotation) else { return nil }
        let identifier = "marker"
        var pinView: MKPinAnnotationView
        
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView {
            dequeuedView.annotation = annotation
            pinView = dequeuedView
        } else {
            pinView = MKPinAnnotationView(annotation: marker, reuseIdentifier: identifier)
            pinView.canShowCallout = true
            pinView.animatesDrop = true
            pinView.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            view.sizeToFit()
        }
        return pinView
    }
    
    // Follow the user position with a delta of 0.05 degrees
    func mapView (_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        let distanceDelta: CLLocationDegrees = 0.05
        let coordinateSpan: MKCoordinateSpan = MKCoordinateSpan.init(latitudeDelta: distanceDelta, longitudeDelta: distanceDelta)
        self.m_map!.setRegion(MKCoordinateRegion.init(center: userLocation.location!.coordinate, span: coordinateSpan), animated: true)
    }
    
    // Show distance with the selected marker in the marker's description
    func mapView (_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        print("A marker was selected with the title: \(view.annotation!.title! ?? "no title")")
        guard let markerAnnotation = (view.annotation as? MKPointAnnotation) else { return }
        if let userLocation = m_map!.userLocation.location {
            let distance = userLocation.distance(from: CLLocation.init(latitude: (markerAnnotation.coordinate.latitude), longitude: (markerAnnotation.coordinate.longitude)))
            let stringDistance = String(format: "%.2f meters", distance)
            markerAnnotation.subtitle = "distance: \(stringDistance)"
            print("The selected marker is situated within a distance of \(distance) meters from the user")
        }
    }
    
    // Play marker's associated video
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if control == view.rightCalloutAccessoryView {
            guard let markerAnnotation = (view.annotation as? MKMyPointAnnotation) else { return }
            if let url = markerAnnotation.videoURL {
                setVideo(videoURL: url)
                print("Playing video associated to the marker with the title: \(view.annotation!.title! ?? "no title")")
            }
        }
    }
    
    // Set video from URL
    func setVideo (videoURL: URL) {
        self.player!.replaceCurrentItem(with: AVPlayerItem.init(url: videoURL))
        self.m_AVPlayerLayer!.frame = (m_AVPlayerLayer?.superlayer?.bounds)!
        self.m_AVPlayerLayer!.videoGravity = .resize
    }
    
    // Play video
    func playVideo () {
        self.player?.play()
    }
    // END-CODE-UOC-4
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
